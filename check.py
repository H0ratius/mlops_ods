import numpy

print(numpy.__version__)


def func(a: int, b: int) -> int:
    return a + b
